# DeepRL Tetris
![](demo.gif)

## Training
  1. Set all your parameters and whatnot in [Config.py](GA3C/ga3c/Config.py)
  2. Run python solver.py
  
## Testing
  Run python matris/matris_with_net.py

## Other repositories used. Big thanks to them so I didn't have to duplicate their hard work.
 - https://github.com/SmartViking/MaTris
 - https://github.com/NVlabs/GA3C