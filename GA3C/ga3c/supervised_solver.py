import pdb
import sys
import os
sys.path.append(os.path.abspath(os.path.dirname(__file__)))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir)))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir, os.pardir, 'matris')))
import tensorflow as tf
from GA3C.ga3c.NetworkVP import NetworkVP
import argparse
from Config import Config
from my_utils.tensorflow_util import tf_util
from my_utils.util import time_util
import h5py
import numpy as np
import cv2
import glob
import time
import threading

from robot_controller import RobotController
import create_dagger_dataset

TEST_ITER = 1000
LOG_ITER = 10
IMAGE_LOG_ITER = 1000
SAVE_ITER = 1000
DEBUG = False
EPOCHS_PER_REFRESH = 100000000
NUM_DATA_THREADS = 0


def one_hot(data, num_categories):
    new_data = np.zeros((list(data.shape) + [num_categories]), dtype=np.float32)
    new_data[np.arange(data.shape[0]), data] = 1
    return new_data


def refresh_dataset(train_data_h5=None, test_data_h5=None):
    if train_data_h5 is not None:
        train_data_h5.close()
        test_data_h5.close()
    train_file = glob.glob('human_recording/train/*.h5')[0]
    train_data_h5 = h5py.File(train_file, 'r')
    train_data = {
        'images': train_data_h5['images'], #[...].astype(np.float32) / 255.0 - 0.5,
        'next_tetromino': train_data_h5['next_tetromino'][...],
        'labels': train_data_h5['labels'][...],
    }
    #train_data_h5.close()
    train_data_size = train_data['images'].shape[0]

    test_file = glob.glob('human_recording/test/*.h5')[0]
    test_data_h5 = h5py.File(test_file, 'r')
    test_data = {
        'images': test_data_h5['images'][...], #.astype(np.float32) / 255.0 - 0.5,
        'next_tetromino': test_data_h5['next_tetromino'][...],
        'labels': test_data_h5['labels'][...],
    }
    #test_data_h5.close()
    test_data_size = test_data['images'].shape[0]
    return train_data, test_data, train_data_size, test_data_size, train_data_h5, test_data_h5


def train(sleep=0):
    try:
        data_generators = []
        for ii in range(NUM_DATA_THREADS):
            with tf.name_scope(('data_generator_' + str(ii))):
                data_generator = RobotController(load=False)
                data_generators.append(data_generator)
        network = NetworkVP(Config.DEVICE, Config.NETWORK_NAME, Config.NUM_ACTIONS, reuse=tf.AUTO_REUSE, log=(not DEBUG))
        start_iter = network.load()
        if len(data_generators) > 0:
            data_generators[0].expert_network.load()

        train_data, test_data, train_data_size, test_data_size, train_data_h5, test_data_h5 = refresh_dataset()
        print('data size', train_data_size, test_data_size)
        assert(train_data_size == len(train_data['images']))
        #train_data = shuffle_data(train_data)
        #test_data = shuffle_data(test_data)

        train_loss_ph = tf.placeholder(tf.float32, None, 'train_loss_ph')
        train_accuracy_ph = tf.placeholder(tf.float32, None, 'train_accuracy_ph')
        train_summary = tf.summary.merge([
                tf.summary.scalar('train_loss', train_loss_ph),
                tf.summary.scalar('train_accuracy', train_accuracy_ph),
        ])

        test_loss_ph = tf.placeholder(tf.float32, None, 'test_loss_ph')
        test_accuracy_ph = tf.placeholder(tf.float32, None, 'test_accuracy_ph')
        test_summary = tf.summary.merge([
            tf.summary.scalar('test_loss', test_loss_ph),
            tf.summary.scalar('test_accuracy', test_accuracy_ph),
        ])
        log_writer = tf.summary.FileWriter("logs/%s/%s" % (network.model_name, time_util.get_time_str()), network.sess.graph)


        total_time = 0
        num_iters = 0
        data_ind = 0
        dataset_lock = threading.Lock()
        image_lock = threading.Lock()

        def create_new_data(thread_ind):
            global train_data, test_data, train_data_size, test_data_size
            data_generator = data_generators[thread_ind]
            while True:
                print('generating new data thread', thread_ind)
                data_generator.run()

        for thread_ind in range(NUM_DATA_THREADS):
            data_thread = threading.Thread(target=create_new_data, args=[thread_ind])
            data_thread.daemon = True
            data_thread.start()
            time.sleep(1)

        start_time = time.time()
        epoch_count = 0
        network.sess.graph.finalize()
        for iteration in range(start_iter, start_iter + Config.MAX_DAGGER_ITERATION):
            if data_ind + Config.DAGGER_BATCH_SIZE > train_data_size:
                data_ind = 0
                epoch_count += 1
                if epoch_count % EPOCHS_PER_REFRESH == 0:
                    dataset_lock.acquire()
                    print('--------------  reloading data ---------------')
                    create_dagger_dataset.main()
                    train_data, test_data, train_data_size, test_data_size, train_data_h5, test_data_h5 = refresh_dataset(train_data_h5, test_data_h5)
                    #train_data = shuffle_data(train_data)
                    print('data size', train_data_size, test_data_size)
                    assert(train_data_size == len(train_data['images']))
                    dataset_lock.release()

            dataset_lock.acquire()
            data_batch = {key: val[data_ind:data_ind + Config.DAGGER_BATCH_SIZE, ...] for key, val in train_data.items()}
            dataset_lock.release()
            data_batch['images'] = data_batch['images'].astype(np.float32) / 255.0 - 0.5
            data_batch['next_tetromino'] = one_hot(data_batch['next_tetromino'], Config.NUM_TETROMINOS)
            data_batch['labels'] = one_hot(data_batch['labels'], Config.NUM_ACTIONS)
            loss, accuracy = network.train_supervised((data_batch['images'], data_batch['next_tetromino']), data_batch['labels'])


            if not DEBUG and iteration % LOG_ITER == 0:
                summary = network.sess.run(train_summary, feed_dict={train_loss_ph: loss, train_accuracy_ph: accuracy})
                log_writer.add_summary(summary, iteration)
                log_writer.flush()

            if not DEBUG and iteration % IMAGE_LOG_ITER == 0:
                summary = network.sess.run(network.variable_summaries)
                log_writer.add_summary(summary, iteration)
                log_writer.flush()
            data_ind += Config.DAGGER_BATCH_SIZE

            if iteration % TEST_ITER == 0:
                test_loss = []
                test_acc = []
                dataset_lock.acquire()
                test_start = time.time()
                for test_data_ind in range(0, test_data_size - Config.DAGGER_BATCH_SIZE + 1, Config.DAGGER_BATCH_SIZE):
                    data_batch = {key: val[test_data_ind:test_data_ind + Config.DAGGER_BATCH_SIZE, ...] for key, val in test_data.items()}
                    data_batch['images'] = data_batch['images'].astype(np.float32) / 255.0 - 0.5
                    data_batch['next_tetromino'] = one_hot(data_batch['next_tetromino'], Config.NUM_TETROMINOS)
                    data_batch['labels'] = one_hot(data_batch['labels'], Config.NUM_ACTIONS)
                    if DEBUG:
                        predictions = network.predict_p((data_batch['images'], data_batch['next_tetromino']))
                        for ii in range(Config.DAGGER_BATCH_SIZE):
                            prediction = predictions[ii, ...]
                            labels = data_batch['labels'][ii, ...]
                            image = data_batch['images'][ii, ..., -1]
                            print('prediction order', np.argsort(prediction)[::-1])
                            print('label', np.argmax(labels))
                            image = ((image + 1) * 128).astype(np.uint8)
                            cv2.namedWindow('image', cv2.WINDOW_NORMAL)
                            cv2.imshow('image', image)
                            cv2.resizeWindow('image', 600, 600)
                            cv2.waitKey(0)
                    else:
                        loss, accuracy = network.get_output_supervised((data_batch['images'], data_batch['next_tetromino']), data_batch['labels'])
                        test_loss.append(loss)
                        test_acc.append(accuracy)
                print('test time     %.3f' % (time.time() - test_start))
                dataset_lock.release()

                if not DEBUG:
                    summary = network.sess.run(test_summary, feed_dict={test_loss_ph: np.mean(test_loss), test_accuracy_ph: np.mean(test_acc)})
                    log_writer.add_summary(summary, iteration)
                    log_writer.flush()
            if iteration % SAVE_ITER == 0 and sleep == 0:
                network.save(iteration)
            if sleep > 0:
                time.sleep(sleep)

            end_time = time.time()
            curr_time = end_time - start_time
            total_time += curr_time
            start_time = time.time()
            num_iters += 1
            if iteration % 100 == 0:
                print('iteration    ', iteration)
                print('data ind     ', data_ind)
                print('accuracy      %.3f' % accuracy)
                print('time          %.3f' % curr_time)
                print('average time  %.3f\n' % (total_time / num_iters))

    except KeyboardInterrupt:
        pass
    except:
        import traceback
        traceback.print_exc()
        pdb.set_trace()
        print('error')
    finally:
        print('saving')
        network.save(iteration)


if __name__ == '__main__':
    train()
