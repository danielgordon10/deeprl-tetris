# Copyright (c) 2016, NVIDIA CORPORATION. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#  * Neither the name of NVIDIA CORPORATION nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import os
import re
import numpy as np
import tensorflow as tf

import sys
import os
sys.path.append(os.path.abspath(os.path.dirname(__file__)))
from Config import Config

import pdb
from my_utils.tensorflow_util import tf_util
from my_utils.util import time_util

UPDATE_V_ONLY = False
LOGS_PER_FULL_LOG = 1000
PI_SCALAR = 1

class NetworkVP:
    SESS = None

    @staticmethod
    def singleton_session():
        if NetworkVP.SESS is None:
            NetworkVP.SESS = tf.Session(
                config=tf.ConfigProto(
                    allow_soft_placement=True,
                    log_device_placement=False,
                    gpu_options=tf.GPUOptions(allow_growth=True)))
        return NetworkVP.SESS

    def __init__(self, device, model_name, num_actions, reuse=False, log=True):
        self.device = device
        self.model_name = model_name
        self.num_actions = num_actions

        self.img_width = Config.IMAGE_WIDTH
        self.img_height = Config.IMAGE_HEIGHT
        self.img_channels = Config.IMAGE_CHANNELS
        self.stacked_frames = Config.STACKED_FRAMES

        self.learning_rate = Config.LEARNING_RATE_START
        self.beta = Config.BETA_START
        self.log_epsilon = Config.LOG_EPSILON
        self.log_count = 0
        self.should_log = log

        with tf.device(self.device):
            with tf.variable_scope(self.model_name, reuse=reuse):
                self._create_graph()

        self.sess = NetworkVP.singleton_session()
        self.sess.run(tf.global_variables_initializer())

        if Config.TENSORBOARD and self.should_log:
            self._create_tensor_board()

        if Config.LOAD_CHECKPOINT or Config.SAVE_MODELS:
            vars = tf.global_variables()
            self.saver = tf.train.Saver(vars, max_to_keep=3)

    def _create_graph(self):
        self.x = tf.placeholder(
            tf.float32, [None, self.img_height, self.img_width, self.img_channels, self.stacked_frames], name='X')
        self.y_r = tf.placeholder(tf.float32, [None], name='Yr')
        self.action_index = tf.placeholder(tf.float32, [None, self.num_actions])

        self.var_beta = tf.placeholder(tf.float32, name='beta', shape=[])
        self.var_learning_rate = tf.placeholder(tf.float32, name='lr', shape=[])

        self.global_step = tf.Variable(0, trainable=False, name='step')

        inputs = tf_util.remove_axis(self.x, axis=4)

        self.next_piece_placeholder = tf.placeholder(tf.float32, name='next_piece_placeholder', shape=[None, 7])
        self.held_piece_placeholder = tf.placeholder(tf.float32, name='next_piece_placeholder', shape=[None, 8])

        next_piece_embed = tf.contrib.layers.fully_connected(self.next_piece_placeholder, 16, activation_fn=tf.nn.elu, scope='next_piece_embed')
        held_piece_embed = tf.contrib.layers.fully_connected(self.held_piece_placeholder, 16, activation_fn=tf.nn.elu, scope='held_piece_embed')

        outs = []
        if self.model_name == 'full_net' or self.model_name == 'supervised' or self.model_name == 'square_convs':
            regular_convs = tf.contrib.layers.conv2d(inputs, 32, 5, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv1_1')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 32, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv1_2')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 32, 1, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv1_3')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 32, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv1_4')
            regular_convs1 = tf.contrib.layers.max_pool2d(regular_convs, 3, 2, padding='VALID', scope='regular_pool1')
            outs.append(tf.contrib.layers.flatten(regular_convs1))

            regular_convs = tf.contrib.layers.conv2d(regular_convs1, 64, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv2_1')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 32, 1, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv2_2')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 64, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv2_3')
            regular_convs2 = tf.contrib.layers.max_pool2d(regular_convs, 2, 2, padding='VALID', scope='regular_pool2')
            outs.append(tf.contrib.layers.flatten(regular_convs2))

            regular_convs = tf.contrib.layers.conv2d(regular_convs2, 128, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv3_1')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 64, 1, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv3_2')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 128, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv3_3')
            regular_convs3 = tf.contrib.layers.max_pool2d(regular_convs, 2, 2, padding='VALID', scope='regular_pool3')
            outs.append(tf.contrib.layers.flatten(regular_convs3))

        if self.model_name == 'full_net' or self.model_name == 'supervised' or self.model_name == 'row_convs':

            full_row_convs1 = tf.contrib.layers.conv2d(inputs, 32, [1, inputs.shape[2]], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_row_convs1')
            full_col_convs1 = tf.contrib.layers.conv2d(inputs, 32, [inputs.shape[1], 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_col_convs1')

            full_row_convs2 = tf.contrib.layers.conv2d(regular_convs1, 32, [1, regular_convs1.shape[2]], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_row_convs2')
            full_col_convs2 = tf.contrib.layers.conv2d(regular_convs1, 32, [regular_convs1.shape[1], 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_col_convs2')

            full_row_convs3 = tf.contrib.layers.conv2d(regular_convs2, 32, [1, regular_convs2.shape[2]], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_row_convs3')
            full_col_convs3 = tf.contrib.layers.conv2d(regular_convs2, 32, [regular_convs2.shape[1], 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_col_convs3')

            outs.append(tf.contrib.layers.flatten(full_row_convs1))
            outs.append(tf.contrib.layers.flatten(full_row_convs2))
            outs.append(tf.contrib.layers.flatten(full_row_convs3))
            outs.append(tf.contrib.layers.flatten(full_col_convs1))
            outs.append(tf.contrib.layers.flatten(full_col_convs2))
            outs.append(tf.contrib.layers.flatten(full_col_convs3))

        if self.model_name == 'line_convs':
            outs = []
            net = tf.contrib.layers.conv2d(inputs, 32, [4, 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='conv_col1')
            net = tf.contrib.layers.conv2d(net, 32, [1, 4], 1, padding='VALID', activation_fn=tf.nn.elu, scope='conv_row1')
            net = tf.contrib.layers.conv2d(net, 32, [4, 1], 1, padding='SAME', activation_fn=tf.nn.elu, scope='conv_col2')
            net = tf.contrib.layers.conv2d(net, 32, [1, 4], 1, padding='SAME', activation_fn=tf.nn.elu, scope='conv_row2')
            net = tf.contrib.layers.conv2d(net, 32, [4, 1], 1, padding='SAME', activation_fn=tf.nn.elu, scope='conv_col3')
            net = tf.contrib.layers.conv2d(net, 32, [1, 4], 1, padding='SAME', activation_fn=tf.nn.elu, scope='conv_row3')
            full_col_convs1 = tf.contrib.layers.conv2d(net, 32, [net.shape[1], 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_col_convs1')
            full_row_convs1 = tf.contrib.layers.conv2d(net, 32, [1, net.shape[2]], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_row_convs1')
            outs.append(tf.contrib.layers.flatten(full_col_convs1))
            outs.append(tf.contrib.layers.flatten(full_row_convs1))

        elif self.model_name == 'small_net':
            outs = []
            regular_convs = tf.contrib.layers.conv2d(inputs, 32, 5, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv1_1')
            regular_convs = tf.contrib.layers.conv2d(regular_convs, 32, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv1_2')
            regular_convs1 = tf.contrib.layers.max_pool2d(regular_convs, 3, 2, padding='VALID', scope='regular_pool1')

            regular_convs = tf.contrib.layers.conv2d(regular_convs1, 32, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv2_1')
            regular_convs2 = tf.contrib.layers.max_pool2d(regular_convs, 2, 2, padding='VALID', scope='regular_pool2')

            regular_convs = tf.contrib.layers.conv2d(regular_convs2, 32, 3, 1, padding='SAME', activation_fn=tf.nn.elu, scope='regular_conv3_1')
            regular_convs3 = tf.contrib.layers.max_pool2d(regular_convs, 2, 2, padding='VALID', scope='regular_pool3')

            full_row_convs1 = tf.contrib.layers.conv2d(inputs, 8, [1, inputs.shape[2]], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_row_convs1')
            full_col_convs1 = tf.contrib.layers.conv2d(inputs, 8, [inputs.shape[1], 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_col_convs1')

            full_row_convs2 = tf.contrib.layers.conv2d(regular_convs1, 8, [1, regular_convs1.shape[2]], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_row_convs2')
            full_col_convs2 = tf.contrib.layers.conv2d(regular_convs1, 8, [regular_convs1.shape[1], 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_col_convs2')

            full_row_convs3 = tf.contrib.layers.conv2d(regular_convs2, 8, [1, regular_convs2.shape[2]], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_row_convs3')
            full_col_convs3 = tf.contrib.layers.conv2d(regular_convs2, 8, [regular_convs2.shape[1], 1], 1, padding='VALID', activation_fn=tf.nn.elu, scope='full_col_convs3')

            outs.append(tf.contrib.layers.flatten(regular_convs1))
            outs.append(tf.contrib.layers.flatten(regular_convs2))
            outs.append(tf.contrib.layers.flatten(regular_convs3))
            outs.append(tf.contrib.layers.flatten(full_row_convs1))
            outs.append(tf.contrib.layers.flatten(full_row_convs2))
            outs.append(tf.contrib.layers.flatten(full_row_convs3))
            outs.append(tf.contrib.layers.flatten(full_col_convs1))
            outs.append(tf.contrib.layers.flatten(full_col_convs2))
            outs.append(tf.contrib.layers.flatten(full_col_convs3))

        elif self.model_name == 'fc_net':
            inputs = tf.contrib.layers.flatten(inputs)
            fc0 = tf.contrib.layers.fully_connected(inputs, 1024, activation_fn=tf.nn.elu, scope='fc0')
            outs.append(fc0)


        outs.extend([next_piece_embed, held_piece_embed])
        big_concat = tf.concat(outs, axis=1)
        fc1 = tf.contrib.layers.fully_connected(big_concat, 256, activation_fn=tf.nn.elu, scope='fc1')
        fc2 = tf.contrib.layers.fully_connected(fc1, 256, activation_fn=tf.nn.elu, scope='fc2')

        if UPDATE_V_ONLY:
            self.logits_v = tf.contrib.layers.fully_connected(tf.stop_gradient(fc2), 1, activation_fn=None, scope='logits_v')[:,0]
        else:
            self.logits_v = tf.contrib.layers.fully_connected(fc2, 1, activation_fn=None, scope='logits_v')[:,0]
        self.cost_v = tf.losses.huber_loss(self.y_r, self.logits_v)
        self.logits_p = tf.contrib.layers.fully_connected(fc2, self.num_actions, activation_fn=None, scope='logits_p')


        '''
        # As implemented in A3C paper
        self.n1 = self.conv2d_layer(tf_util.remove_axis(self.x, axis=3), 8, 16, 'conv11', strides=[1, 4, 4, 1])
        self.n2 = self.conv2d_layer(self.n1, 4, 32, 'conv12', strides=[1, 2, 2, 1])
        _input = self.n2

        flatten_input_shape = _input.get_shape()
        nb_elements = flatten_input_shape[1] * flatten_input_shape[2] * flatten_input_shape[3]

        self.flat = tf.reshape(_input, shape=[-1, nb_elements._value])
        self.d1 = self.dense_layer(self.flat, 256, 'dense1')

        self.logits_v = tf.squeeze(self.dense_layer(self.d1, 1, 'logits_v', func=None), axis=[1])
        self.cost_v = 0.5 * tf.reduce_sum(tf.square(self.y_r - self.logits_v), axis=0)

        self.logits_p = self.dense_layer(self.d1, self.num_actions, 'logits_p', func=None)
        '''

        if Config.USE_LOG_SOFTMAX:
            self.softmax_p = tf.nn.softmax(self.logits_p)
            self.log_softmax_p = tf.nn.log_softmax(self.logits_p)
            self.log_selected_action_prob = tf.reduce_sum(self.log_softmax_p * self.action_index, axis=1)

            self.cost_p_1 = self.log_selected_action_prob * (self.y_r - tf.stop_gradient(self.logits_v))
            self.cost_p_2 = -1 * self.var_beta * \
                        tf.reduce_sum(self.log_softmax_p * self.softmax_p, axis=1)
        else:
            self.softmax_p = (tf.nn.softmax(self.logits_p) + Config.MIN_POLICY) / (1.0 + Config.MIN_POLICY * self.num_actions)
            self.selected_action_prob = tf.reduce_sum(self.softmax_p * self.action_index, axis=1)

            self.cost_p_1 = tf.log(tf.maximum(self.selected_action_prob, self.log_epsilon)) \
                        * (self.y_r - tf.stop_gradient(self.logits_v))
            self.cost_p_2 = -1 * self.var_beta * \
                        tf.reduce_sum(tf.log(tf.maximum(self.softmax_p, self.log_epsilon)) *
                                      self.softmax_p, axis=1)
        
        self.cost_p_1_agg = tf.reduce_mean(self.cost_p_1, axis=0)
        self.cost_p_2_agg = tf.reduce_mean(self.cost_p_2, axis=0)
        self.cost_p = - PI_SCALAR * (self.cost_p_1_agg + self.cost_p_2_agg)
        if UPDATE_V_ONLY:
            self.cost_p = 0


        if Config.DUAL_RMSPROP:
            self.opt_p = tf.train.RMSPropOptimizer(
                learning_rate=self.var_learning_rate,
                decay=Config.RMSPROP_DECAY,
                momentum=Config.RMSPROP_MOMENTUM,
                epsilon=Config.RMSPROP_EPSILON)

            self.opt_v = tf.train.RMSPropOptimizer(
                learning_rate=self.var_learning_rate,
                decay=Config.RMSPROP_DECAY,
                momentum=Config.RMSPROP_MOMENTUM,
                epsilon=Config.RMSPROP_EPSILON)
        else:
            self.cost_all = self.cost_p + 100 * self.cost_v
            self.opt = tf.train.RMSPropOptimizer(
                learning_rate=self.var_learning_rate,
                decay=Config.RMSPROP_DECAY,
                momentum=Config.RMSPROP_MOMENTUM,
                epsilon=Config.RMSPROP_EPSILON)

        if Config.USE_GRAD_CLIP:
            if Config.DUAL_RMSPROP:
                self.opt_grad_v = self.opt_v.compute_gradients(100 * self.cost_v)
                self.opt_grad_v_clipped = [(tf.clip_by_norm(g, Config.GRAD_CLIP_NORM),v) 
                                            for g,v in self.opt_grad_v if not g is None]
                self.train_op_v = self.opt_v.apply_gradients(self.opt_grad_v_clipped)
            
                self.opt_grad_p = self.opt_p.compute_gradients(self.cost_p)
                self.opt_grad_p_clipped = [(tf.clip_by_norm(g, Config.GRAD_CLIP_NORM),v)
                                            for g,v in self.opt_grad_p if not g is None]
                self.train_op_p = self.opt_p.apply_gradients(self.opt_grad_p_clipped)
                self.train_op = [self.train_op_p, self.train_op_v]
            else:
                self.opt_grad = self.opt.compute_gradients(self.cost_all)
                self.opt_grad_clipped = [(tf.clip_by_average_norm(g, Config.GRAD_CLIP_NORM),v) for g,v in self.opt_grad]
                self.train_op = self.opt.apply_gradients(self.opt_grad_clipped)
        else:
            if Config.DUAL_RMSPROP:
                self.train_op_v = self.opt_p.minimize(100 * self.cost_v, global_step=self.global_step)
                self.train_op_p = self.opt_v.minimize(self.cost_p, global_step=self.global_step)
                self.train_op = [self.train_op_p, self.train_op_v]
            else:
                self.train_op = self.opt.minimize(self.cost_all, global_step=self.global_step)

        if Config.SUPERVISED:
            self.dagger_opt = tf.train.AdamOptimizer(learning_rate=Config.DAGGER_LEARNING_RATE)
            self.supervised_loss = tf.losses.softmax_cross_entropy(self.action_index, self.logits_p)
            self.dagger_train_op = self.dagger_opt.minimize(self.supervised_loss)
            preds = tf.argmax(self.logits_p, axis=-1)
            action_index = tf.argmax(self.action_index, axis=-1)
            self.accuracy = tf.reduce_mean(tf.to_float(tf.equal(preds, action_index)))


    def _create_tensor_board(self):
        summaries = tf.get_collection(tf.GraphKeys.SUMMARIES)
        summaries.append(tf.summary.scalar("Pcost_advantage", self.cost_p_1_agg))
        summaries.append(tf.summary.scalar("Pcost_entropy", self.cost_p_2_agg))
        summaries.append(tf.summary.scalar("Pcost", self.cost_p))
        summaries.append(tf.summary.scalar("Vcost", self.cost_v))
        summaries.append(tf.summary.scalar("LearningRate", self.var_learning_rate))
        summaries.append(tf.summary.scalar("Beta", self.var_beta))
        self.game_score = tf.placeholder(tf.float32, None, name='game_score_ph')
        self.episode_length = tf.placeholder(tf.float32, None, name='episode_length_ph')
        self.line_counts = tf.placeholder(tf.float32, [5], name='line_counts')
        self.episode_summary = tf.summary.merge((
                tf.summary.scalar("game_stats/game_score", self.game_score),
                tf.summary.scalar("game_stats/episode_length", self.episode_length),
                tf.summary.scalar("game_stats/line_count_total", self.line_counts[0]),
                tf.summary.scalar("game_stats/line_count_1", self.line_counts[1]),
                tf.summary.scalar("game_stats/line_count_2", self.line_counts[2]),
                tf.summary.scalar("game_stats/line_count_3", self.line_counts[3]),
                tf.summary.scalar("game_stats/line_count_4", self.line_counts[4])))
        '''
        summaries.append(tf.summary.histogram("activation_n1", self.n1))
        summaries.append(tf.summary.histogram("activation_n2", self.n2))
        summaries.append(tf.summary.histogram("activation_d2", self.d1))
        summaries.append(tf.summary.histogram("activation_v", self.logits_v))
        summaries.append(tf.summary.histogram("activation_p", self.softmax_p))
        '''

        self.summary_op = tf.summary.merge(summaries)

        self.variable_summaries = []
        for var in tf.trainable_variables():
            #summaries.append(tf.summary.histogram("weights_%s" % var.name, var))
            if 'conv' in var.name and len(var.shape) == 4:
                self.variable_summaries.append(tf_util.conv_variable_summaries(var, var.name[:-2]))
            else:
                self.variable_summaries.append(tf_util.variable_summaries(var, var.name[:-2]))

        self.summary_op_full = tf.summary.merge(summaries + self.variable_summaries)

        self.variable_summaries = tf.summary.merge(self.variable_summaries)

        self.log_writer = tf.summary.FileWriter("logs/%s/%s" % (self.model_name, time_util.get_time_str()), self.sess.graph)

    '''
    def dense_layer(self, input, out_dim, name, func=tf.nn.relu):
        in_dim = input.get_shape().as_list()[-1]
        d = 1.0 / np.sqrt(in_dim)
        with tf.variable_scope(name):
            w_init = tf.random_uniform_initializer(-d, d)
            b_init = tf.random_uniform_initializer(-d, d)
            w = tf.get_variable('w', dtype=tf.float32, shape=[in_dim, out_dim], initializer=w_init)
            b = tf.get_variable('b', shape=[out_dim], initializer=b_init)

            output = tf.matmul(input, w) + b
            if func is not None:
                output = func(output)

        return output

    def conv2d_layer(self, input, filter_size, out_dim, name, strides, func=tf.nn.relu):
        in_dim = input.get_shape().as_list()[-1]
        d = 1.0 / np.sqrt(filter_size * filter_size * in_dim)
        with tf.variable_scope(name):
            w_init = tf.random_uniform_initializer(-d, d)
            b_init = tf.random_uniform_initializer(-d, d)
            w = tf.get_variable('w',
                                shape=[filter_size, filter_size, in_dim, out_dim],
                                dtype=tf.float32,
                                initializer=w_init)
            b = tf.get_variable('b', shape=[out_dim], initializer=b_init)

            output = tf.nn.conv2d(input, w, strides=strides, padding='SAME') + b
            if func is not None:
                output = func(output)

        return output
    '''

    def __get_base_feed_dict(self):
        return {self.var_beta: self.beta, self.var_learning_rate: self.learning_rate}

    def get_global_step(self):
        step = self.sess.run(self.global_step)
        return step

    def predict_single(self, inputs):
        return self.predict_p((inputs[0][None, :], inputs[1][None, :]))[0]

    def predict_v(self, inputs):
        prediction = self.sess.run(self.logits_v, feed_dict={self.x: inputs[0], self.next_piece_placeholder: inputs[1],
                                                             self.held_piece_placeholder: inputs[2]})
        return prediction

    def predict_p(self, inputs):
        prediction = self.sess.run(self.softmax_p, feed_dict={self.x: inputs[0], self.next_piece_placeholder: inputs[1],
                                                              self.held_piece_placeholder: inputs[2]})
        return prediction
    
    def predict_p_and_v(self, inputs):
        return self.sess.run([self.softmax_p, self.logits_v], feed_dict={self.x: inputs[0],
                                                                         self.next_piece_placeholder: inputs[1],
                                                                         self.held_piece_placeholder: inputs[2]})

    def train(self, inputs, y_r, a, trainer_id):
        feed_dict = self.__get_base_feed_dict()
        feed_dict.update({self.x: inputs[0], self.next_piece_placeholder: inputs[1],
                          self.held_piece_placeholder: inputs[2],
                          self.y_r: y_r, self.action_index: a})
        self.sess.run(self.train_op, feed_dict=feed_dict)

    def train_supervised(self, inputs, labels):
        feed_dict = {self.x: inputs[0], self.next_piece_placeholder: inputs[1],
                     self.held_piece_placeholder: inputs[2],
                     self.action_index: labels}
        loss, accuracy, _ = self.sess.run([self.supervised_loss, self.accuracy, self.dagger_train_op], feed_dict=feed_dict)
        return loss, accuracy

    def get_output_supervised(self, inputs, labels):
        feed_dict = {self.x: inputs[0], self.next_piece_placeholder: inputs[1],
                     self.held_piece_placeholder: inputs[2],
                     self.action_index: labels}
        loss, accuracy = self.sess.run([self.supervised_loss, self.accuracy], feed_dict=feed_dict)
        return loss, accuracy

    def log(self, inputs, y_r, a):
        feed_dict = self.__get_base_feed_dict()
        feed_dict.update({self.x: inputs[0], self.next_piece_placeholder: inputs[1],
                          self.held_piece_placeholder: inputs[2],
                          self.y_r: y_r, self.action_index: a})
        if self.log_count % LOGS_PER_FULL_LOG == 0:
            step, summary = self.sess.run([self.global_step, self.summary_op_full], feed_dict=feed_dict)
        else:
            step, summary = self.sess.run([self.global_step, self.summary_op], feed_dict=feed_dict)
        self.log_writer.add_summary(summary, step)
        self.log_count += 1
        self.log_writer.flush()

    '''
    def _checkpoint_filename(self, episode):
        return 'checkpoints/model.ckpt'
    
    def _get_episode_from_filename(self, filename):
        # TODO: hacky way of getting the episode. ideally episode should be stored as a TF variable
        return int(re.split('/|_|\.', filename)[2])
    '''

    def save(self, episode):
        if not os.path.exists('checkpoints/' + self.model_name):
            os.makedirs('checkpoints/' + self.model_name)
        self.saver.save(self.sess, 'checkpoints/' + self.model_name + '/model.ckpt', global_step=episode)

    def load(self):
        path = 'checkpoints/' + self.model_name
        print('loading ', path)
        #filename = tf.train.latest_checkpoint(os.path.dirname(self._checkpoint_filename(episode=0)))
        episode = tf_util.restore_from_dir(self.sess, path)
        '''
        if Config.LOAD_EPISODE > 0:
            filename = self._checkpoint_filename(Config.LOAD_EPISODE)
        self.saver.restore(self.sess, filename)
        return self._get_episode_from_filename(filename)
        '''
        return episode

    '''
    def get_variables_names(self):
        return [var.name for var in self.graph.get_collection('trainable_variables')]

    def get_variable_value(self, name):
        return self.sess.run(self.graph.get_tensor_by_name(name))
    '''
