import pdb
import h5py
from my_utils.util import time_util
import glob
import numpy as np
import scipy.misc
import random
import os
import shutil

def main():
    num_frames = 2
    image_shape = (22, 10, 3)
    for mode in ['train', 'test']:
        folders = set(glob.glob('human_recording/' + mode + '/data/*/'))
        #images = []
        data = []
        num_images_total = 0
        used_folders = set()
        for folder in folders:
            if not os.path.exists(folder + 'images/data.npz'):
                continue
            used_folders.add(folder)
            data_on = np.load(folder + 'images/data.npz')
            num_images = data_on['images'].shape[0]
            num_images_total += (num_images - num_frames + 1)
            #folder_ims = sorted(glob.glob(folder + 'images/*.png'))
            #images.extend([(len(data), ff) for ff in range(num_images - num_frames)])
            dd = [data_on['images'][start:num_images-num_frames + start + 1].copy() for start in range(num_frames)]
            data.append({key: val for (key, val) in data_on.items()})
            data[-1]['images'] = np.stack(dd, axis=-1)
        prev_dataset_file = sorted(glob.glob('human_recording/' + mode + '/*.h5'))
        prev_dataset_size = 0
        if len(prev_dataset_file) > 0:
            prev_dataset_file = prev_dataset_file[0]
            prev_dataset = h5py.File(prev_dataset_file, 'r')
            prev_dataset_size = len(prev_dataset['images'])


        print('num images', num_images_total)
        dataset = h5py.File('human_recording/' + mode + '/dagger_' + time_util.get_time_str() + '.h5', 'w')

        dataset.create_dataset('images', [prev_dataset_size + num_images_total] + list(image_shape) + [num_frames], dtype=np.uint8)
        dataset.create_dataset('next_tetromino', [prev_dataset_size + num_images_total], dtype=np.uint8)
        dataset.create_dataset('labels', [prev_dataset_size + num_images_total], dtype=np.uint8)

        #random.shuffle(images)
        #im_inds = np.random.permutation(np.arange(num_images_total - num_frames + 1))
        #im_inds = np.arange(num_images_total)
        ii = 0
        #while ii < num_images_total:
        for data_on in data:
            print('image %d / %d %d%%' % (ii, num_images_total, int(100 * ii / num_images_total)))
            #image_positions = images[ii]
            #data_on = data[image_positions[0]]
            num_images = data_on['images'].shape[0]
            dataset['images'][ii:ii + num_images, ...] = data_on['images']
            dataset['next_tetromino'][ii:ii + num_images] = data_on['next_tetromino'][num_frames - 1:]
            dataset['labels'][ii:ii + num_images] = data_on['labels'][num_frames - 1:]
            ii += num_images

        '''

        for ii, im_ind in enumerate(im_inds):
            if ii % 100 == 0:
                print('image %d / %d %d%%' % (ii, num_images_total, int(100 * ii / num_images_total)))
            image_positions = images[im_ind]
            data_on = data[image_positions[0]]
            image_data = data_on['images'][image_positions[1],...]
            action = data_on['labels'][image_positions[1] + num_frames - 1]
            next_tetromino = data_on['next_tetromino'][image_positions[1] + num_frames - 1]
            dataset['images'][ii,...] = image_data
            dataset['next_tetromino'][ii] = next_tetromino
            dataset['labels'][ii] = action
        '''

        all_keys = []
        for key in dataset.keys():
            if type(dataset[key]) == h5py._hl.group.Group:
                all_keys.extend([key + '/' + k2 for k2 in dataset[key].keys()])
            else:
                all_keys.append(key)

        if len(prev_dataset_file) > 0:
            for key in all_keys:
                dataset[key][num_images_total:,...] = prev_dataset[key][...]
            prev_dataset.close()
            os.remove(prev_dataset_file)

        if mode == 'train':
            # shuffle the data
            num_data_points = dataset['images'].shape[0]
            random_order = np.random.permutation(np.arange(num_data_points))
            for key in all_keys:
                print('shuffling', key)
                data = dataset[key][...]
                dataset[key][...] = data[random_order, ...]

        # Clean up
        dataset.flush()
        dataset.close()
        for folder in used_folders:
            shutil.rmtree(folder)

if __name__ == '__main__':
    main()






