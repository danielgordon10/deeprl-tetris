from GA3C.ga3c.Config import Config
import GA3C.ga3c.supervised_solver as solver
if Config.SUPERVISED and Config.DAGGER:
    solver.train()
else:
    import GA3C.ga3c.main_train as train
    train.run()
