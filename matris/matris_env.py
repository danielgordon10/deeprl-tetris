#!/usr/bin/env python
import pdb
import time
import numpy as np
from collections import deque, Counter



import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__))))
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from GA3C.ga3c.Config import Config

import random

import tetrominoes
from tetrominoes import list_of_tetrominoes
from tetrominoes import list_of_start_tetrominoes
from tetrominoes import rotate

from scores import load_score, write_score


class GameOver(Exception):
    """Exception used for its control flow properties"""

#def get_sound(filename):
    #return pygame.mixer.Sound(os.path.join(os.path.dirname(__file__), "resources", filename))

BGCOLOR = (15, 15, 20)
BORDERCOLOR = (140, 140, 140)

BLOCKSIZE = 30
BORDERWIDTH = 10

MATRIS_OFFSET = 20

MATRIX_WIDTH = 10
MATRIX_HEIGHT = 22

LEFT_MARGIN = 340

WIDTH = MATRIX_WIDTH*BLOCKSIZE + BORDERWIDTH*2 + MATRIS_OFFSET*2 + LEFT_MARGIN
HEIGHT = (MATRIX_HEIGHT-2)*BLOCKSIZE + BORDERWIDTH*2 + MATRIS_OFFSET*2

VISIBLE_MATRIX_HEIGHT = MATRIX_HEIGHT - 2

class Matris(object):
    def __init__(self):
        #self.surface = screen.subsurface(Rect((MATRIS_OFFSET+BORDERWIDTH, MATRIS_OFFSET+BORDERWIDTH),
                                              #(MATRIX_WIDTH * BLOCKSIZE, (MATRIX_HEIGHT-2) * BLOCKSIZE)))

        self.matrix = dict()
        for y in range(MATRIX_HEIGHT):
            for x in range(MATRIX_WIDTH):
                self.matrix[(y,x)] = None
        """
        `self.matrix` is the current state of the tetris board, that is, it records which squares are
        currently occupied. It does not include the falling tetromino. The information relating to the
        falling tetromino is managed by `self.set_tetrominoes` instead. When the falling tetromino "dies",
        it will be placed in `self.matrix`.
        """

        # Use TGM random piece
        self.next_tetromino = random.choice(list_of_start_tetrominoes)
        self.prev_tetromino_bag = deque()
        self.prev_tetromino_bag.append(tetrominoes.T_left_snake[0])
        self.prev_tetromino_bag.append(tetrominoes.T_right_snake[0])
        self.prev_tetromino_bag.append(tetrominoes.T_left_snake[0])
        self.prev_tetromino_bag.append(self.next_tetromino[0])
        self.prev_tetromino_counter = Counter()
        self.prev_tetromino_counter[tetrominoes.T_left_snake[0]] = 2
        self.prev_tetromino_counter[tetrominoes.T_right_snake[0]] = 1
        self.prev_tetromino_counter[self.next_tetromino[0]] = 1
        self.held_tetromino = None
        self.recently_swapped = False

        self.set_tetrominoes()
        self.tetromino_rotation = 0
        #self.downwards_timer = 0
        self.base_downwards_speed = 0.01 # Move down every 400 ms

        self.movement_keys = {'left': 0, 'right': 0}
        self.movement_keys_speed = 0.05
        #self.movement_keys_timer = (-self.movement_keys_speed)*2

        self.level = 1
        self.score = 0
        self.lines = 0
        self.line_count = [0] * 5

        self.combo = 1 # Combo will increase when you clear lines with several tetrominos in a row

        self.paused = False

        self.highscore = load_score()
        self.played_highscorebeaten_sound = False

        #self.levelup_sound  = get_sound("levelup.wav")
        #self.gameover_sound = get_sound("gameover.wav")
        #self.linescleared_sound = get_sound("linecleared.wav")
        #self.highscorebeaten_sound = get_sound("highscorebeaten.wav")


    def set_tetrominoes(self):
        self.current_tetromino = self.next_tetromino
        for _ in range(6):
            self.next_tetromino = random.choice(list_of_tetrominoes)
            if self.prev_tetromino_counter[self.next_tetromino[0]] == 0:
                break
        last_tetromino = self.prev_tetromino_bag.popleft()
        self.prev_tetromino_counter[last_tetromino] -= 1
        self.prev_tetromino_bag.append(self.next_tetromino[0])
        self.prev_tetromino_counter[self.next_tetromino[0]] += 1

        #self.surface_of_next_tetromino = self.construct_surface_of_next_tetromino()
        self.tetromino_position = (0,4) if len(self.current_tetromino.shape) == 2 else (0, 3)
        self.tetromino_rotation = 0
        self.tetromino_block = self.block(self.current_tetromino.color)
        self.shadow_block = self.block(self.current_tetromino.color, shadow=True)
        self.recently_swapped = False

    def swap_held(self):
        if self.recently_swapped:
            return False
        if self.held_tetromino is None:
            # Hold current piece, promote next piece
            self.held_tetromino = self.current_tetromino
            self.set_tetrominoes()
            self.recently_swapped = True
        else:
            tmp = self.held_tetromino
            self.held_tetromino = self.current_tetromino
            self.current_tetromino = tmp

            self.tetromino_position = (0,4) if len(self.current_tetromino.shape) == 2 else (0, 3)
            self.tetromino_rotation = 0
            self.tetromino_block = self.block(self.current_tetromino.color)
            self.shadow_block = self.block(self.current_tetromino.color, shadow=True)
            self.recently_swapped = True
        return True



    def hard_drop(self):
        amount = 0
        while self.request_movement('down'):
            amount += 1
        self.score += 10 * amount

        self.lock_tetromino()


    def update(self, action):
        self.needs_redraw = False

        #pressed = lambda key: event.type == pygame.KEYDOWN and event.key == key
        #unpressed = lambda key: event.type == pygame.KEYUP and event.key == key

        '''
        events = pygame.event.get()

        for event in events:
            if pressed(pygame.K_p):
                #self.surface.fill((0,0,0))
                self.needs_redraw = True
                self.paused = not self.paused
            elif event.type == pygame.QUIT:
                self.gameover(full_exit=True)
            elif pressed(pygame.K_ESCAPE):
                self.gameover()
        '''

        if self.paused:
            return self.needs_redraw

        if action == 0:
            # do nothing
            #self.hard_drop()
            pass
        elif action == 1:
            self.request_movement('left')
        elif action == 2:
            self.request_movement('right')
        elif action == 3:
            self.request_forward_rotation()
        elif action == 4:
            self.request_reverse_rotation()
        elif action == 5:
            #pass
            self.request_movement('down')
        elif action == 6:
            #pass
            self.hard_drop()
        elif action == 7:
            self.swap_held()

        '''
        for event in events:
            if pressed(pygame.K_SPACE):
                self.hard_drop()
            elif pressed(pygame.K_UP) or pressed(pygame.K_w):
                self.request_forward_rotation()

            elif pressed(pygame.K_LEFT) or pressed(pygame.K_a):
                self.request_movement('left')
                self.movement_keys['left'] = 1
            elif pressed(pygame.K_RIGHT) or pressed(pygame.K_d):
                self.request_movement('right')
                self.movement_keys['right'] = 1

            elif unpressed(pygame.K_LEFT) or unpressed(pygame.K_a):
                self.movement_keys['left'] = 0
                #self.movement_keys_timer = (-self.movement_keys_speed)*2
            elif unpressed(pygame.K_RIGHT) or unpressed(pygame.K_d):
                self.movement_keys['right'] = 0
                #self.movement_keys_timer = (-self.movement_keys_speed)*2




        self.downwards_speed = self.base_downwards_speed ** (1 + self.level/10.)

        #self.downwards_timer += timepassed
        #downwards_speed = self.downwards_speed*0.10 if any([pygame.key.get_pressed()[pygame.K_DOWN],
                                                            #pygame.key.get_pressed()[pygame.K_s]]) else self.downwards_speed
        #if self.downwards_timer > downwards_speed:
        '''
        if not self.request_movement('down'):
            self.lock_tetromino()

            #self.downwards_timer %= downwards_speed


        #if any(self.movement_keys.values()):
            #self.movement_keys_timer += timepassed
        #if self.movement_keys_timer > self.movement_keys_speed:
            #self.request_movement('right' if self.movement_keys['right'] else 'left')
            #self.movement_keys_timer %= self.movement_keys_speed

        return self.needs_redraw

    def draw_surface(self):
        with_tetromino = self.blend(matrix=self.place_shadow())
        self.full_board = np.zeros((MATRIX_HEIGHT, MATRIX_WIDTH, 3), dtype=np.uint8)
        self.board = self.full_board[:,:,0]
        self.curr_piece = self.full_board[:,:,1]
        self.shadow_board = self.full_board[:,:,2]

        for y in range(MATRIX_HEIGHT):
            for x in range(MATRIX_WIDTH):
                #                                       I hide the 2 first rows by drawing them outside of the surface
                #block_location = Rect(x*BLOCKSIZE, (y*BLOCKSIZE - 2*BLOCKSIZE), BLOCKSIZE, BLOCKSIZE)
                if with_tetromino[(y,x)] is None:
                    #self.surface.fill(BGCOLOR, block_location)
                    pass
                else:
                    if with_tetromino[(y,x)][0] == 'shadow':
                        #self.surface.fill(BGCOLOR, block_location)
                        self.shadow_board[y,x] = 1
                    else:
                        self.curr_piece[y,x] = 1
                    #self.surface.blit(with_tetromino[(y,x)][1], block_location)
                if self.matrix[(y,x)] is not None:
                    self.board[y,x] = 1
        self.curr_piece -= self.board

    def gameover(self, full_exit=False):
        """
        Gameover occurs when a new tetromino does not fit after the old one has died, either
        after a "natural" drop or a hard drop by the player. That is why `self.lock_tetromino`
        is responsible for checking if it's game over.
        """
        '''
        if self.level <= 8:
            self.score -= 8000
        else:
            self.score += 8000
        '''
        self.score -= 100000

        #write_score(self.score)
        if Config.DEBUG:
            print('Game over, score', self.score)

        if full_exit:
            exit()
        else:
            raise GameOver("Sucker!")

    def place_shadow(self):
        posY, posX = self.tetromino_position
        while self.blend(position=(posY, posX)):
            posY += 1

        position = (posY-1, posX)

        return self.blend(position=position, shadow=True)

    def fits_in_matrix(self, shape, position):
        posY, posX = position
        for x in range(posX, posX+len(shape)):
            for y in range(posY, posY+len(shape)):
                if self.matrix.get((y, x), False) is False and shape[y-posY][x-posX]: # outside matrix
                    return False

        return position

    def request_rotation(self, direction):
        rotation = (self.tetromino_rotation + direction) % 4
        shape = self.rotated(rotation)

        y, x = self.tetromino_position

        position = (self.fits_in_matrix(shape, (y, x)) or
                    self.fits_in_matrix(shape, (y, x+1)) or
                    self.fits_in_matrix(shape, (y, x-1)) or
                    self.fits_in_matrix(shape, (y, x+2)) or
                    self.fits_in_matrix(shape, (y, x-2)))
        # ^ That's how wall-kick is implemented

        if position and self.blend(shape, position):
            self.tetromino_rotation = rotation
            self.tetromino_position = position

            self.needs_redraw = True
            return self.tetromino_rotation
        else:
            return False

    def request_forward_rotation(self):
        self.request_rotation(1)

    def request_reverse_rotation(self):
        self.request_rotation(-1)

    def request_movement(self, direction):
        posY, posX = self.tetromino_position
        if direction == 'left' and self.blend(position=(posY, posX-1)):
            self.tetromino_position = (posY, posX-1)
            self.needs_redraw = True
            return self.tetromino_position
        elif direction == 'right' and self.blend(position=(posY, posX+1)):
            self.tetromino_position = (posY, posX+1)
            self.needs_redraw = True
            return self.tetromino_position
        elif direction == 'up' and self.blend(position=(posY-1, posX)):
            self.needs_redraw = True
            self.tetromino_position = (posY-1, posX)
            return self.tetromino_position
        elif direction == 'down' and self.blend(position=(posY+1, posX)):
            self.needs_redraw = True
            self.tetromino_position = (posY+1, posX)
            return self.tetromino_position
        else:
            return False

    def rotated(self, rotation=None):
        if rotation is None:
            rotation = self.tetromino_rotation
        return rotate(self.current_tetromino.shape, rotation)

    def block(self, color, shadow=False):
        colors = {'blue':   (47, 64, 224),
                  'yellow': (225, 242, 41),
                  'pink':   (242, 41, 195),
                  'green':  (22, 181, 64),
                  'red':    (204, 22, 22),
                  'orange': (245, 144, 12),
                  'cyan':   (10, 255, 226)}


        if shadow:
            end = [40] # end is the alpha value
        else:
            end = [] # Adding this to the end will not change the array, thus no alpha value

        '''
        border = Surface((BLOCKSIZE, BLOCKSIZE), pygame.SRCALPHA, 32)
        border.fill(list(map(lambda c: c*0.5, colors[color])) + end)

        borderwidth = 2

        box = Surface((BLOCKSIZE-borderwidth*2, BLOCKSIZE-borderwidth*2), pygame.SRCALPHA, 32)
        boxarr = pygame.PixelArray(box)
        for x in range(len(boxarr)):
            for y in range(len(boxarr)):
                boxarr[x][y] = tuple(list(map(lambda c: min(255, int(c*random.uniform(0.8, 1.2))), colors[color])) + end)

        del boxarr # deleting boxarr or else the box surface will be 'locked' or something like that and won't blit.
        #border.blit(box, Rect(borderwidth, borderwidth, 0, 0))


        return border
        '''

    def lock_tetromino(self):
        """
        This method is called whenever the falling tetromino "dies". `self.matrix` is updated,
        the lines are counted and cleared, and a new tetromino is chosen.
        """
        if not self.blend():
            # Problem
            self.gameover()
        self.matrix = self.blend()

        lines_cleared = self.remove_lines()
        self.lines += lines_cleared

        self.score += 10

        if lines_cleared:
            #if lines_cleared >= 4:
                #self.linescleared_sound.play()
            self.line_count[0] += lines_cleared
            self.line_count[lines_cleared] += 1
            self.score += 100 * (lines_cleared**2) #* self.combo
            if lines_cleared == 4:
                self.score += 400 * (lines_cleared**2) #* self.combo

            #print('cleared line')

            '''
            if not self.played_highscorebeaten_sound and self.score > self.highscore:
                if self.highscore != 0:
                    self.highscorebeaten_sound.play()
                self.played_highscorebeaten_sound = True
            '''

        if self.lines >= self.level*10:
            #self.levelup_sound.play()
            self.level += 1
        #if self.level > 8:
            #self.gameover()


        #self.combo = self.combo + 1 if lines_cleared else 1

        self.set_tetrominoes()

        if not self.blend():
            #self.gameover_sound.play()
            self.gameover()

        self.needs_redraw = True

    def remove_lines(self):
        try:
            lines = []
            for y in range(MATRIX_HEIGHT):
                line = (y, [])
                for x in range(MATRIX_WIDTH):
                    if self.matrix[(y,x)]:
                        line[1].append(x)
                if len(line[1]) == MATRIX_WIDTH:
                    lines.append(y)

            for line in sorted(lines):
                for x in range(MATRIX_WIDTH):
                    self.matrix[(line,x)] = None
                for y in range(0, line+1)[::-1]:
                    for x in range(MATRIX_WIDTH):
                        self.matrix[(y,x)] = self.matrix.get((y-1,x), None)
        except:
            import traceback
            traceback.print_exc()
            pdb.set_trace()
            print('bad')
        return len(lines)

    def blend(self, shape=None, position=None, matrix=None, shadow=False):
        """
        Does `shape` at `position` fit in `matrix`? If so, return a new copy of `matrix` where all
        the squares of `shape` have been placed in `matrix`. Otherwise, return False.

        This method is often used simply as a test, for example to see if an action by the player is valid.
        It is also used in `self.draw_surface` to paint the falling tetromino and its shadow on the screen.
        """
        if shape is None:
            shape = self.rotated()
        if position is None:
            position = self.tetromino_position

        copy = dict(self.matrix if matrix is None else matrix)
        posY, posX = position
        for x in range(posX, posX+len(shape)):
            for y in range(posY, posY+len(shape)):
                if (copy.get((y, x), False) is False and shape[y-posY][x-posX] # shape is outside the matrix
                    or # coordinate is occupied by something else which isn't a shadow
                    copy.get((y,x)) and shape[y-posY][x-posX] and copy[(y,x)][0] != 'shadow'):

                    return False # Blend failed; `shape` at `position` breaks the matrix

                elif shape[y-posY][x-posX]:
                    copy[(y,x)] = ('shadow', self.shadow_block) if shadow else ('block', self.tetromino_block)

        return copy

    def construct_surface_of_next_tetromino(self):
        shape = self.next_tetromino.shape
        surf = Surface((len(shape)*BLOCKSIZE, len(shape)*BLOCKSIZE), pygame.SRCALPHA, 32)

        for y in range(len(shape)):
            for x in range(len(shape)):
                if shape[y][x]:
                    surf.blit(self.block(self.next_tetromino.color), (x*BLOCKSIZE, y*BLOCKSIZE))
        return surf

class Game(object):
    def __init__(self):
        self.matris = Matris()
        self.terminal = False
        self.num_frames = 0
        self.total_num_frames = 0
        self.total_time = 0
        self.total_curr_time = 0
        self.score = 0
        self.curr_score = 0
        self.curr_occluded = 0
        self.action_space = ['do_nothing', 'left', 'right', 'forward_rot', 'reverse_rot', 'down', 'drop', 'hold']
        self.piece_names = [tetromino[0] for tetromino in list_of_tetrominoes]
        self.tetromino_ind = {tetromino[0] : ii for ii, tetromino in enumerate(list_of_tetrominoes)}

    def reset(self, seed=None):
        self.terminal = False
        if seed is not None:
            random.seed(seed)
        self.matris = Matris()
        self.num_frames = 0
        self.total_time = 0
        self.total_curr_time = 0
        self.score = 0
        self.curr_score = 0
        self.curr_occluded = 0
        self.redraw()
        return self.frame, self.next_tetromino_one_hot, self.held_tetromino_one_hot

    def random_play(self):
        import cv2
        while True:
            self.reset()
            while not self.terminal:
                action_ind = random.randint(0, 3)
                self.step(action_ind)
                cv2.imshow('state', self.frame * 255)
                cv2.waitKey(1)
            #print('frame', self.num_frames)

    def step(self, action_ind):
        self.num_frames += 1
        self.total_num_frames += 1
        t_start = time.time()
        try:
            if self.matris.update(action_ind):
                self.redraw()
        except GameOver:
            self.terminal = True
            #self.redraw()

        if np.sum(self.matris.board) > 0:
            occlusion_height = np.argmax(self.matris.board, axis=0)
            occlusion_height[occlusion_height == 0] = self.matris.board.shape[0]
            occluded_height = self.matris.board.shape[0] - np.argmin(np.flipud(self.matris.board), axis=0)
            occlusion_score = occluded_height - occlusion_height

            '''

            occlusion_score = np.argmax(self.matris.board, axis=0)
            # reverse sign
            occlusion_score[occlusion_score != 0] = self.matris.board.shape[0] - occlusion_score[occlusion_score != 0]
            occlusion_score -= np.sum(self.matris.board, axis=0).astype(np.int32)
            '''
            occlusion_score = np.sum(occlusion_score)
            occlusion_delta = (occlusion_score - self.curr_occluded)
            if occlusion_delta > 0:
                occlusion_delta = occlusion_delta * 20 + 300
            self.matris.score -= occlusion_delta
            self.curr_occluded = occlusion_score

        self.curr_score = self.matris.score / 1000.0 - self.score
        self.score = self.matris.score / 1000.0

        t_end = time.time()
        t_diff = t_end - t_start
        self.total_time += t_diff
        self.total_curr_time += t_diff
        '''
        if self.num_frames % 50 == 0:
            print('curr framerate  %d' % int(50.0 / self.total_curr_time))
            print('total framerate %d' % int(self.num_frames / self.total_time))
        '''
        return self.frame, self.next_tetromino_one_hot, self.held_tetromino_one_hot, self.curr_score, self.terminal, self.matris.line_count

    def redraw(self):
        self.matris.draw_surface()
        self.frame = self.matris.full_board
        self.next_tetromino_one_hot = np.zeros(len(self.tetromino_ind))
        self.next_tetromino_one_hot[self.tetromino_ind[self.matris.next_tetromino[0]]] = 1
        self.held_tetromino_one_hot = np.zeros(len(self.tetromino_ind) + 1)
        if self.matris.held_tetromino is not None:
            self.held_tetromino_one_hot[self.tetromino_ind[self.matris.held_tetromino[0]]] = 1
        self.held_tetromino_one_hot[-1] = int(self.matris.recently_swapped)

        '''
        if Config.DEBUG:
            if not os.path.exists('visualizations/images'):
                os.makedirs('visualizations/images')
            frame = cv2.resize(self.frame * 255, (self.frame.shape[1] * 10, self.frame.shape[0] * 10),
                             interpolation=cv2.INTER_NEAREST)
            scipy.misc.imsave('visualizations/images/%09d.jpg' % self.total_num_frames, frame)
        '''
        '''
        cv2.imshow('board', self.matris.board * 255)
        cv2.imshow('shadow', self.matris.shadow_board * 255)
        cv2.imshow('curr_piece', self.matris.curr_piece * 255)
        cv2.waitKey(1)
        '''
        '''
        if not self.matris.paused:
            tricky_centerx = WIDTH-(WIDTH-(MATRIS_OFFSET+BLOCKSIZE*MATRIX_WIDTH+BORDERWIDTH*2))/2

            nextts = self.next_tetromino_surf(self.matris.surface_of_next_tetromino)
            screen.blit(nextts, nextts.get_rect(top=MATRIS_OFFSET, centerx=tricky_centerx))

            infos = self.info_surf()
            screen.blit(infos, infos.get_rect(bottom=HEIGHT-MATRIS_OFFSET, centerx=tricky_centerx))

            self.matris.draw_surface()

        pygame.display.flip()
        '''


    def info_surf(self):

        textcolor = (255, 255, 255)
        font = pygame.font.Font(None, 30)
        width = (WIDTH-(MATRIS_OFFSET+BLOCKSIZE*MATRIX_WIDTH+BORDERWIDTH*2)) - MATRIS_OFFSET*2

        def renderpair(text, val):
            text = font.render(text, True, textcolor)
            val = font.render(str(val), True, textcolor)

            surf = Surface((width, text.get_rect().height + BORDERWIDTH*2), pygame.SRCALPHA, 32)

            surf.blit(text, text.get_rect(top=BORDERWIDTH+10, left=BORDERWIDTH+10))
            surf.blit(val, val.get_rect(top=BORDERWIDTH+10, right=width-(BORDERWIDTH+10)))
            return surf

        scoresurf = renderpair("Score", self.matris.score)
        levelsurf = renderpair("Level", self.matris.level)
        linessurf = renderpair("Lines", self.matris.lines)
        combosurf = renderpair("Combo", "x{}".format(self.matris.combo))

        height = 20 + (levelsurf.get_rect().height +
                       scoresurf.get_rect().height +
                       linessurf.get_rect().height +
                       combosurf.get_rect().height )

        area = Surface((width, height))
        area.fill(BORDERCOLOR)
        area.fill(BGCOLOR, Rect(BORDERWIDTH, BORDERWIDTH, width-BORDERWIDTH*2, height-BORDERWIDTH*2))

        area.blit(levelsurf, (0,0))
        area.blit(scoresurf, (0, levelsurf.get_rect().height))
        area.blit(linessurf, (0, levelsurf.get_rect().height + scoresurf.get_rect().height))
        area.blit(combosurf, (0, levelsurf.get_rect().height + scoresurf.get_rect().height + linessurf.get_rect().height))

        return area

    def next_tetromino_surf(self, tetromino_surf):
        area = Surface((BLOCKSIZE*5, BLOCKSIZE*5))
        area.fill(BORDERCOLOR)
        area.fill(BGCOLOR, Rect(BORDERWIDTH, BORDERWIDTH, BLOCKSIZE*5-BORDERWIDTH*2, BLOCKSIZE*5-BORDERWIDTH*2))

        areasize = area.get_size()[0]
        tetromino_surf_size = tetromino_surf.get_size()[0]
        # ^^ I'm assuming width and height are the same

        center = areasize/2 - tetromino_surf_size/2
        area.blit(tetromino_surf, (center, center))

        return area

def construct_nightmare(size):
    surf = Surface(size)

    boxsize = 8
    bordersize = 1
    vals = '1235' # only the lower values, for darker colors and greater fear
    arr = pygame.PixelArray(surf)
    for x in range(0, len(arr), boxsize):
        for y in range(0, len(arr[x]), boxsize):

            color = int(''.join([random.choice(vals) + random.choice(vals) for _ in range(3)]), 16)

            for LX in range(x, x+(boxsize - bordersize)):
                for LY in range(y, y+(boxsize - bordersize)):
                    if LX < len(arr) and LY < len(arr[x]):
                        arr[LX][LY] = color
    del arr
    return surf


if __name__ == '__main__':
    #pygame.init()

    #screen = pygame.display.set_mode((WIDTH, HEIGHT))
    #screen = None
    #pygame.display.set_caption("MaTris")
    #Menu().main(screen)
    game = Game()
    game.reset(0)
    game.random_play()

