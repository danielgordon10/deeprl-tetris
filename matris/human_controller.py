import pdb
import cv2
import numpy as np
import scipy.misc
import matris_env
import os
import time
from my_utils.util import time_util
import random

import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from GA3C.ga3c.Config import Config
from GA3C.ga3c.NetworkVP import NetworkVP

FRAME_SCALE = 20
USE_NETWORK_PROB = 0.1

def rescale(img, scale=FRAME_SCALE):
    return cv2.resize(img, (FRAME_SCALE * img.shape[1], FRAME_SCALE * img.shape[0]), interpolation=cv2.INTER_NEAREST)

env = matris_env.Game()

base_folder = 'human_recording'

frame_ind = 0
img_path_base = os.path.join(base_folder, time_util.get_time_str(), 'images')
os.makedirs(img_path_base)
env.reset()
terminal = False
action = 0
network = NetworkVP(Config.DEVICE, Config.NETWORK_NAME, Config.NUM_ACTIONS)
network.load()

input_image = [env.matris.full_board.copy() for _ in range(Config.STACKED_FRAMES)]

def get_next_action(network):
    global input_image
    input_image = [input_image[1], env.matris.full_board.copy()]
    env.next_tetromino_one_hot = np.zeros(len(env.tetromino_ind))
    env.next_tetromino_one_hot[env.tetromino_ind[env.matris.next_tetromino[0]]] = 1
    input_image_np = np.stack(input_image, axis=-1).astype(np.float32) - 0.5
    probs = network.predict_p((input_image_np[np.newaxis, ...], env.next_tetromino_one_hot[np.newaxis, ...]))
    next_action = np.random.choice(Config.NUM_ACTIONS, p=probs[0, :])
    return next_action

while not terminal:
    frame_ind += 1
    if random.random() < USE_NETWORK_PROB:
        new_action = get_next_action(network)
        if not (new_action != action and new_action == Config.NUM_ACTIONS - 1):
            # Don't let it drop on me
            action = new_action
    frame_orig, next_tetromino_one_hot, score, terminal, line_count = env.step(action)
    next_tetromino_ind = np.argmax(next_tetromino_one_hot)
    frame_orig *= 255
    frame = rescale(frame_orig)
    cv2.imshow('board', frame)
    next_tetromino = (np.array(env.matris.next_tetromino.shape) == 'X').astype(np.uint8) * 255
    nex_tetromino = rescale(next_tetromino)
    cv2.imshow('next_piece', next_tetromino)
    key = cv2.waitKey(0)
    print('key', key)
    if key == 97 or key == 81:
        # a
        action = 1
    elif key == 114 or key == 84:
        # r
        action = 5
    elif key == 115 or key == 83:
        # s
        action = 2
    elif key == 119 or key == 82:
        # w
        action = 3
    elif key == 32:
        # space
        action = 6
    scipy.misc.imsave(os.path.join(img_path_base, '%07d_%03d_%03d.png' % (frame_ind, next_tetromino_ind, action)), frame_orig)










