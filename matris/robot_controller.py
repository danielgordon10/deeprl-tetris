import pdb
import cv2
import numpy as np
import scipy.misc
import matris_env
import tensorflow as tf
import os
import time
from my_utils.util import time_util
import random
import threading

import sys
import os
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
from GA3C.ga3c.Config import Config
from GA3C.ga3c.NetworkVP import NetworkVP

FRAME_SCALE = 20
USE_NETWORK_PROB = 1
DEBUG = True


def rescale(img, scale=FRAME_SCALE):
    return cv2.resize(img, (FRAME_SCALE * img.shape[1], FRAME_SCALE * img.shape[0]), interpolation=cv2.INTER_NEAREST)


class RobotController(object):

    def __init__(self, load=True, num_threads=10):
        self.envs = [None] * num_threads
        self.network = NetworkVP(Config.DEVICE, Config.NETWORK_NAME, Config.NUM_ACTIONS, reuse=tf.AUTO_REUSE, log=False)
        if load:
            self.network.load()

        self.expert_network = NetworkVP(Config.DEVICE, 'square_convs', Config.NUM_ACTIONS, reuse=tf.AUTO_REUSE, log=False)
        if load:
            self.expert_network.load()

        self.input_image = None
        self.num_threads = num_threads
        self.lock = threading.Lock()

    def get_next_action(self, network, thread_ind):
        self.envs[thread_ind].next_tetromino_one_hot = np.zeros(len(self.envs[thread_ind].tetromino_ind))
        self.envs[thread_ind].next_tetromino_one_hot[self.envs[thread_ind].tetromino_ind[self.envs[thread_ind].matris.next_tetromino[0]]] = 1
        input_image_np = np.stack(self.input_image[thread_ind], axis=-1).astype(np.float32) - 0.5
        probs = network.predict_p((input_image_np[np.newaxis, ...], self.envs[thread_ind].next_tetromino_one_hot[np.newaxis, ...]))
        next_action = np.random.choice(Config.NUM_ACTIONS, p=probs[0, :])
        return next_action

    def run(self, num_runs=10):
        self.input_image = [[[] for _ in range(Config.STACKED_FRAMES)] for jj in range(self.num_threads)]
        work = list(range(num_runs))

        def thread_run(thread_ind):
            self.envs[thread_ind] = matris_env.Game()
            while len(work) > 0:
                game_ind = -1
                self.lock.acquire()
                if len(work) > 0:
                    game_ind = work.pop()
                self.lock.release()
                if game_ind == -1:
                    break
                if game_ind % 10 == 0:
                    mode = 'test'
                else:
                    mode = 'train'
                base_folder = 'human_recording/' + mode + '/data/'
                images = []
                next_tetrominos = []
                labels = []
                print('game ind', game_ind, 'thread_ind', thread_ind)
                frame_ind = 0
                terminal = False
                action = 0
                total_score = 0
                self.envs[thread_ind].reset()
                self.input_image[thread_ind] = [self.envs[thread_ind].matris.full_board.copy() for _ in range(Config.STACKED_FRAMES)]
                while not terminal:
                    frame_ind += 1
                    if random.random() < USE_NETWORK_PROB:
                        new_action = self.get_next_action(self.network, thread_ind)
                        action = new_action
                        '''
                        if not (new_action != action and new_action == Config.NUM_ACTIONS - 1):
                            # Don't let it drop on me
                            action = new_action
                        '''
                    frame_orig, next_tetromino_one_hot, score, terminal, line_count = self.envs[thread_ind].step(action)
                    total_score += score
                    self.input_image[thread_ind] = [self.input_image[thread_ind][1], self.envs[thread_ind].matris.full_board.copy()]
                    next_tetromino_ind = int(np.argmax(next_tetromino_one_hot))
                    frame_orig *= 255
                    next_tetromino = (np.array(self.envs[thread_ind].matris.next_tetromino.shape) == 'X').astype(np.uint8) * 255
                    if DEBUG:
                        frame = rescale(frame_orig)
                        next_tetromino = rescale(next_tetromino)
                        cv2.imshow('board_%d' % thread_ind, frame)
                        cv2.imshow('next_piece_%d' % thread_ind, next_tetromino)
                        key = cv2.waitKey(1)
                        #print('key', key)
                        if key == 97 or key == 81:
                            # a
                            action = 1
                        elif key == 114 or key == 84:
                            # r
                            action = 5
                        elif key == 115 or key == 83:
                            # s
                            action = 2
                        elif key == 119 or key == 82:
                            # w
                            action = 3
                        elif key == 32:
                            # space
                            action = 6
                    action = self.get_next_action(self.expert_network, thread_ind)
                    images.append(frame_orig.astype(np.uint8))
                    next_tetrominos.append(next_tetromino_ind)
                    labels.append(action)
                    #scipy.misc.imsave(os.path.join(img_path_base, '%07d_%03d_%03d.png' % (frame_ind, next_tetromino_ind, action)), frame_orig)
                print('score %.3f' % total_score)
                img_path_base = os.path.join(base_folder, time_util.get_time_str() + str(random.randint(0,2**32)), 'images')
                os.makedirs(img_path_base)
                np.savez(os.path.join(img_path_base, 'data.npz'), images=np.array(images), labels=np.array(labels),
                        next_tetromino=np.array(next_tetrominos))

        threads = []
        if self.num_threads == 1:
            thread_run(0)
        else:
            for ii in range(self.num_threads):
                thread = threading.Thread(target=thread_run, args=[ii])
                threads.append(thread)
                thread.start()
                time.sleep(1)

            for ii in range(self.num_threads):
                threads[ii].join()


if __name__ == '__main__':
    if DEBUG:
        robot_controller = RobotController(num_threads=1)
    else:
        robot_controller = RobotController()
    robot_controller.run(100)






